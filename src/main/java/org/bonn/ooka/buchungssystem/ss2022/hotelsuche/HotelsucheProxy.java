package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.sql.Time;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelsucheProxy implements Hotelsuche{

    private Hotelsuche hotelsuche;
    private Caching<Hotel> caching;

    public HotelsucheProxy(Caching<Hotel> caching){
        this.caching = caching;
    }

    @Override
    public Hotel[] getHotelByName(String name, Caching<Hotel> caching) {
        if(hotelsuche == null) {
            System.out.println(Instant.now().toString() + ": Zugriffsversuch auf Buchungssystem gescheitert.");
            throw new IllegalStateException("Session has to be opened first!");
        }
        if (caching == null){
            caching = new Caching<Hotel>() {
                @Override
                public void cacheResult(String key, List<Hotel> value) {
                    return;
                }

                @Override
                public List<Hotel> searchCache(String key) {
                    return new ArrayList<Hotel>();
                }
            };
        }
        System.out.println(Instant.now().toString() + ": Zugriff auf Buchungssystem über Methode getHotelByName. Suchwort: " + name);
        return hotelsuche.getHotelByName(name, caching);
    }

    @Override
    public void openSession() {
        hotelsuche = new HotelRetrieval(new DBAccess());
        hotelsuche.openSession();
        System.out.println(Instant.now().toString() + ": Eröffne Session mit Buchungssystem über Interface Hotelsuche");
    }

    @Override
    public void closeSessions() {
        hotelsuche.closeSessions();
        hotelsuche = null;
        System.out.println(Instant.now().toString() + ": Schliesse Session mit Buchungssystem über Interface Hotelsuche");
    }
}
