package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.lang.reflect.Type;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;

public class HotelsuchePort{

    private Caching<Hotel> caching;

    public Class[] providedInterfaces(){
        return new Class[]{Hotelsuche.class, HotelsucheErweitert.class};
    }

    public Object getInterface(int index){
        switch(index){
            case 0:
                return new HotelsucheProxy(caching);
            case 1:
                return new HotelsucheErweitertProxy();
        }
        throw new IllegalArgumentException(String.format("interface with index %d does not exist", index));
    }

    public void setCaching(Caching<Hotel> caching){
        this.caching = caching;
    }
}
