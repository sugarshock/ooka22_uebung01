package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.util.ArrayList;

class HotelRetrieval implements Hotelsuche, HotelsucheErweitert{

    DBAccess dbAccess;


    public HotelRetrieval(DBAccess dbAccess){
        this.dbAccess = dbAccess;
    }

    @java.lang.Override
    public Hotel[] getHotelByName(java.lang.String name, Caching<Hotel> caching) {
        var strings = dbAccess.getObjects(DBAccess.HOTEL, name);
        ArrayList<Hotel> list = new ArrayList<Hotel>();
        for(int i = 0; i < strings.size(); i += 3){
            //quick and dirty, def needs some exception handling lol
            list.add(new Hotel(Integer.parseInt(strings.get(i)), strings.get(i+1), strings.get(i+2) ));
        }
        caching.cacheResult(name, list);
        return list.toArray(new Hotel[0]);
    }

    @java.lang.Override
    public void openSession() {
        dbAccess.openConnection();
    }

    @Override
    public void closeSessions() {
        dbAccess.closeConnection();
    }

    @java.lang.Override
    public Hotel[] getHotelErweitert(java.lang.String[] filters) {

        var name = (filters != null && filters.length > 0) ? filters[0] : "";

        var strings = dbAccess.getObjects(DBAccess.HOTEL, name);
        ArrayList<Hotel> list = new ArrayList<Hotel>();
        for(int i = 0; i < strings.size(); i += 3){
            // do some fancy filtering stuff
            //quick and dirty, def needs some exception handling lol
            list.add(new Hotel(Integer.parseInt(strings.get(i)), strings.get(i+1), strings.get(i+2) ));
        }
        return list.toArray(new Hotel[0]);
    }
}
