package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

public interface HotelsucheErweitert {
    public Hotel[] getHotelErweitert(String[] filters);
    public void openSession();
    public void closeSessions();
}
