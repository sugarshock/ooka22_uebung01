package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

public class Hotel {
    private int id;
    private String name;
    private String ort;

    public Hotel(int id, String name, String ort){
        this.id = id;
        this.name = name;
        this.ort = ort;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOrt() {
        return ort;
    }
}
