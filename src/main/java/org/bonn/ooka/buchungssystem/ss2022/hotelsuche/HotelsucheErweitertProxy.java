package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.time.Instant;

public class HotelsucheErweitertProxy implements HotelsucheErweitert{

    public HotelsucheErweitert hotelsuche;


    @Override
    public Hotel[] getHotelErweitert(String[] filters) {
        if(hotelsuche == null){
            System.out.println(Instant.now().toString() + ": Zugriffsversuch auf Buchungssystem gescheitert.");
            throw new IllegalStateException("Session has to be opened first!");
        }
        System.out.println(Instant.now().toString() + ": Zugriff auf Buchungssystem über Methode getHotelErweitert. Suchfilter: " + filters);
        return hotelsuche.getHotelErweitert(filters);
    }

    @Override
    public void openSession() {
        hotelsuche = new HotelRetrieval(new DBAccess());
        hotelsuche.openSession();
        System.out.println(Instant.now().toString() + ": Eröffne Session mit Buchungssystem über Interface Hotelsuche");
    }

    @Override
    public void closeSessions() {
        hotelsuche.closeSessions();
        System.out.println(Instant.now().toString() + ": Schliesse Session mit Buchungssystem über Interface Hotelsuche");
    }
}
