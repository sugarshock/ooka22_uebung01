package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SimpleHotelCaching implements Caching<Hotel>{
    private HashMap<String, List<Hotel>> cache = new HashMap<>();

    @Override
    public void cacheResult(String key, List<Hotel> value) {
        cache.put(key, value);
    }

    @Override
    public List<Hotel> searchCache(String key) {
        if (cache.containsKey(key))
            return cache.get(key);
        else
            return new ArrayList<>();
    }
}
