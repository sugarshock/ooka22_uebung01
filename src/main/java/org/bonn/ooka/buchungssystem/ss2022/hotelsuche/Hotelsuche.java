package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

public interface Hotelsuche {
    public Hotel[] getHotelByName(String name, Caching<Hotel> caching);
    public void openSession();
    public void closeSessions();
}
