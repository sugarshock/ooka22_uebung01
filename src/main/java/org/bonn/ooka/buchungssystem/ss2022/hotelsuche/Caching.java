package org.bonn.ooka.buchungssystem.ss2022.hotelsuche;

import java.util.List;

public interface Caching<T> {
    public void cacheResult(String key, List<T> value);

    public List<T> searchCache(String key);
}
